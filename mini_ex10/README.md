#mini_ex10

**Individual flowchart for mini_ex6**

 ![ScreenShot](individualflowchart.png)

 

link: https://cdn.staticaly.com/gl/LiseEmilia/ap2019/raw/master/mini_ex6/empty-example/index.html

**TANSA: THE ARTIFICIAL AI BOT**

 ![ScreenShot](MX10TANSA.png)

We have the idea of making our own artificial AI bot called TANSA. We want to write the answers
of the AI bot as text lines in a JSON file together with sound files of us reading the lines aloud. 
All in the group will use their voices to record the lines.
This is to include both male and female voices and thereby to contribute to the equality debate in digital culture. 
This is also to break with the manipulation of how consumers perceive information given from different genders. 
Male voices have a tendency to carry more authority. Female voice tends to be more persuasive.
We are not skilled enough to make a real interaction with AI. So, one of the main challenges 
of this program is that we want to imitate a mic input interaction with our TANSA bot giving the
illusion of the bot answering your question. To do that, we might have to make a conditional 
statement saying that the button has to be pressed for at least 2 seconds before the 
bot fetches an answer. While pressing the mic button a throbber will indicate that the button 
is pressed. We are going to use the boolean statement of mousePressed meaning that the mic
button has to be held down and then released before the JSON data is called. Another option 
for constraining the JSON data access is to let the mic input decide it. So, if the program 
captures a specific volume level the JSON data is accessed and fetched.
We have called our AI bot ‘TANSA’ which is an acronym for There Are No Stupid Answers. 
We want to make a critical statement with this program, by somewhat ridiculing the way we 
tend to lay our trust in machines. So, we basically want people to reconsider their use of 
machines and make them engage with machines with a more critical sense, 
not taking everything for gospel truth.



**The sound of automation**

  ![ScreenShot](MX10automation.png)

When is art really art? Does it have to be created by a human or can a machine generate art? 
We want to test this with a program that generates a piece of digital art combined with music. 
We as creators of the program will set some rules for the program to execute and thereby give
the machine the possibility to create a piece of art on its own. This means that there are still 
humans behind it, making us the overall authors. We want to create a program that generates art
inspired by Mathias’ mini_ex7 where he created a generative art piece consisting of ellipses and 
frames of other shapes appearing when the ellipses would reach specific points. We have used similar
design elements by making ellipses appear from the middle of the canvas, moving randomly and creating a 
randomly generated pattern. But we have redesigned the behavior of it crossing the borders. 
When it reaches either the bottom, top, right or left border a sound of a chord will play and 
a geometric shape will appear, continuously growing in size. One of the challenges with this
idea is that we are unsure of how to implement the sound output when the ellipses reach the borders


**Individual: How is this flowchart different from the one that you had in # 3 (in terms of the role of a flowchart)?**
This flowchart is different in the sense that this one I made, so that it visualize how my program/game works. This is set up 
in the order that appears if you play the game. I played my game and wrote down steps and then created the flowchart. 
The two flowchart we have made as a group for mini_ex10 I see more as having an inspiring effect, since we have not made the 
programs beforhand. So it is like a visualization of out ideas. That is the biggest difference. 

**Individual: If you have to bring the concept of algorithms from flowcharts to a wider cultural context, how would you reflect upon 
the notion of algorithms? (see if you could refer to the text and articulate your thoughts?)**

An algorithm can be compaired to a flowchart in the sense that the purpose for both of them is somewhat the same. 
They are both seen as a form of recipe or instrucktion. These are to be exeucuted and have a pattern of rules to follow.
What they both want to do is visualize what to do. This sounds fairly simple, but it is noth without complexity that can 
be hard to unerstand how algorithms works and how to create them. Flowcharts are easier for humans to understands. And it is 
therefor often used for explainening a program between humans. 
As Bucher wrote: “From a technical standpoint, creating an algorithm is about breaking the problem
down as efficiently as possible, which implies a careful planning of the steps to be taken and their sequence.” 
(Bucher 2018, p 6) The way you create an algorithm is a longer and more thoughtful process. But it is still the 
same mindset we use when creating a flowchart. Understanding this is like understanding the laws and norms of our society. 
It can be quite complex, but when you get it, it makes much sense. So algorithms is all around us, we just don't 
see them since we are so used to it. 



References
Bucher, Taina. "The Multiplicity of Algorithms". If...THEN: Algorithmic Power and Politics, Oxford University Press, 2018
