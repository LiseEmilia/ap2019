#mini_ex8 



![Screenshot](miniex8.png)

link:

https://cdn.staticaly.com/gl/LiseEmilia/ap2019/raw/master/mini_ex8/empty-example/index.html 


Kathrine and I paired up for this mini_ex. 

We have made this piece of interactiv text, that is build on the song
"Aint no sunshine when she's gone". We came up with the idea to find a quote/lyric
and make a function() that made it posible that everytime you click the mouse 
the words changes and so does the meaning of the text. 

We made a JSON-file with 3 arrays, one for each of the words we wanted to 
replace, and made it so that it runs random() and therefor has a lot og 
different combination possiblities. 

We had a hard time combining the 3 arrays to each of the words, but after a few hours
we found this solution. 

