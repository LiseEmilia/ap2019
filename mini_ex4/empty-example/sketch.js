var buttonOn;
var buttonOff;
var mic;
var capture;
let greatvibes;
fontsize = 30;
function preload() {
greatvibes = loadFont('assets/greatvibes.ttf');  }

function setup() {
  textFont(greatvibes);
  createCanvas(800, 800);
  background(100, 169, 229);



  mic = new p5.AudioIn();
  mic.start();
  buttonOn = createButton('Find out');
  buttonOn.position(140, 300);
  buttonOn.style("font-size", "1em");
  buttonOn.style("text-shadow", "0 -1px 0 rgba(0,0,0.02)");
  buttonOn.style("color", "rgba(250)");
  buttonOn.style("border-radius", "10px");
  buttonOn.style("background", "rgba(200,200,200)");
  buttonOff = createButton('Try again');
  buttonOff.position(145, 330);
  buttonOff.style("font-size", "1em");
  buttonOff.style("color", "rgba(250)");
  buttonOff.style("border-radius", "10px");
  buttonOff.style("background", "rgba(200,200,200)");

}
function startcam() { //WEBCAM ON
  capture = createCapture(VIDEO);
  capture.size(265, 200);
  capture.position(230, 155);
  }
function closecam() { //WEBCAM OFF
  capture.hide();
}
function keyPressed() { //scenery mode
  if (keyCode === 32) {
    background(255, 133, 50)
  } else {
    background(250, 200, 100);
  }
}
function draw() {
  fill(244, 248, 255)
  textSize(50);
  textAlign(CENTER);
  text("Mirror, mirror on the wall",350,400);
  text("who is the fairest of them all?",450,450);

  buttonOn.mousePressed(startcam);
  buttonOff.mousePressed(closecam);
  //SOUND
  var vol = mic.getLevel();
    //MIRROR FRAME
  fill(255, 177, 68);
  rect(220, 150, 285, 210, 10);
  //MIRROR GLASS
  fill(232, 230, 227);
  rect(230, 155, 265, 200);

}
