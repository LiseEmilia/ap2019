let playerScore = 0
let paddle
let ball
let bricks
let gameState

function setup() {
  createCanvas(800, 600)

  let colors = createColors()
  gameState = 'playing'
  paddle = new Paddle()
  ball = new Ball(paddle)

  bricks = createBricks(colors)
}

function createColors() {
  const colors = []
 {
    colors.push(color(244, 92, 66))
  }
  return colors
}

function createBricks(colors) {
  const bricks = []
  const rows = 10
  const bricksPerRow = 50
  const brickWidth = width / bricksPerRow
  for (let row = 2; row < rows; row++) {
    for (let i = 0; i < bricksPerRow; i++) {
      brick = new Brick(createVector(brickWidth * i, 25 * row), brickWidth, 25, colors[floor(random(0, colors.length))])
      bricks.push(brick)
    }
  }
  return bricks
}

function draw() {
  if(gameState === 'playing') {
    background(255,44,222)
    ball.bounceEdge()
    ball.bouncePaddle()
    ball.update()

    if (keyIsDown(LEFT_ARROW)) {
      paddle.move('left')
    } else if (keyIsDown(RIGHT_ARROW)) {
      paddle.move('right')
    }

    for (let i = bricks.length - 1; i >= 0; i--) {
      const brick = bricks[i]
      if (brick.isColliding(ball)) {
        ball.reverse('y')
        bricks.splice(i, 1)
        playerScore += brick.points
      } else {
        brick.display()
      }
    }

    paddle.display()
    ball.display()

    if (ball.belowBottom()) {
      gameState = 'OVER'
    }

    if (bricks.length === 1) {
      gameState = 'WON'
    }
  } else {
    textSize(80)

    gameState === 'OVER' ? fill(255) : fill(255)
    text(`GAME ${gameState}!`, width / 2- 240, height / 4)
  }
}
