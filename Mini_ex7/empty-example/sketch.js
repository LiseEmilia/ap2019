let pixels = [];

function setup() {
  createCanvas(800,600);
  background(255);
  pixels[0] = new Pixel()
  frameRate(100);
}

function draw() {
  rectMode(CENTER);

  for (let i = 0; i < pixels.length; i++) {
    pixels[i].show();
    pixels[i].move();

    if (pixels[i].y > height / 4 * 3 + 300) {
      canvasFrame();
      pixels.splice(i, 4);
      pixels.push(new Pixel());
      rdmRect();
    }
    if (pixels[i].y < height / 4 - 500) {
      canvasFrame();
      pixels.splice(i, 1);
      pixels.push(new Pixel());
      rdmTriangle();
    }
    if (pixels[i].x > width / 4 * 3 + 100) {
      canvasFrame();
      pixels.push(new Pixel());
      pixels.splice(i, 1);
      rdmEllipse();
    }
    if (pixels[i].x < width / 4 - 10) {
      canvasFrame();
      pixels.push(new Pixel());
      pixels.splice(i, 1);
      rdmLine();
    }
  }
}
