class Pixel {
  constructor(x, y, r) {
    this.x = width / 2;
    this.y = height / random(1,5);
    this.r = (1,2);
  }
  show() {
    noStroke()
    fill(random(255));
    square(this.x, this.y, this.r *10);
  }
  move() {
    this.x = this.x + random(-20,20);
    this.y = this.y + random(-20,20);
  }

}
