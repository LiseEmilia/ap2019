//mini_ex5

I have changed my thobber so that it has a more smooth effect 
since that was some of the feedback I got on that program. 

Link: https://cdn.staticaly.com/gl/LiseEmilia/ap2019/raw/master/mini_ex5/empty-example/index.html

![Screenshot](mini_ex5.jpg)

Try to contextualize your sketch by answering these:

what is the concept of your work?
My concept was to better my throbber. 
I liked making it and liked the result, but I wanted it to be more smooth.

what is the departure point?
I loved the coulor and effect the throbber already has, but one of the pointers in the feedback 
was to make it more smooth.

what do you want to express?
Since it's a throbber, I wanted to express that something is loading.
But i wanted it to be pleasing and almost mezmerising to look at. 
So times passes faster. 

what have you changed and why?
I have changed the startingpoint so that it now goes back and forward instead of staring over.


Based on the readings that you have done before (see below), What does it mean by programming as a practice? 
To what extend do you agree the concepts that have been illustrate in below readings? 

(Can you draw and link some of the concepts in the text and expand with your critical take?) 
What is the relation between programming and digital culture?


What does it mean by programming as a practice? What is the relation between programming and digital culture?
I think that if we were taught programming-language more in general, we as a society would understand both the
importance of technolgy and the ethics within that. If we know what lays behind the technolgy and software we 
use it would be eaiser for us to use it for the right purpose and understand what influence it has. 

Looking at Annette Vee, and the text coding literacy and her use of computer litteracy. 
I feel that this understates the fact that we need to understand the power of writing in code. 
Does people code to find an form of empowerment? Or it is more used as an creatve outlet? 
There is many questions within this. But no matter what, evolving our coding skills will lead to a more revolutionized
understanding of coding as a laungage. 
