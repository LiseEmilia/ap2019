#mini_ex3 

My throbber 

https://cdn.staticaly.com/gl/LiseEmilia/ap2019/raw/master/mini_ex3/empty-example/index.html  

![ScreenShot](throbberscreen.png)

I wanted to create a visual and aesthetically pleasing throbber.
I started from something more traditional, namely a rotating circle.
I chose to make the colors of the dots with random color but with white as the basic color, 
to get a twinkle effect.I have chosen that there should be only 10 dots 
to allow them to be assembled at the very top before running again.
This gives a good flow, and I feel like it's something I could look at forever, 
and the waiting time might feel a little more fun/calm/relaxing with this 
throbber. 