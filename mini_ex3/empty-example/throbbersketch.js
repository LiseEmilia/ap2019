let k = 99,r=80;
function setup() {
  createCanvas(400, 400);
}

function draw() {
  background(0,0,0);
  translate(width/2,height/2);
  rotate(PI/2);
  noFill();
  stroke(10);
  ellipse(0,0,2*r,2*r);
  for(let i=01;i<10;i++){
     let ang = radians(180*cos(radians(k+i*10)));
     let x = r*cos(ang);
     let y = r*sin(ang);
  			   noStroke();
     fill(random(255),random(255),random(255));
     if((k+i*10)<182)ellipse(x,y,5,5);
  }
  if(k<180)k+=1;
else k =0 ;
}
